/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <gtk/gtk.h>
#include <patahelper/patahelper.h>

namespace {
	void printMats (int parType) {
		std::cout << pata_get_material_cat_desc(parType) << "\n";
		for (int z = 1; z < 5; ++z) {
			std::cout << "\t" << z << " - " << pata_get_material_desc(parType, z) << "\n";
		}
	}
} //unnamed namespace

int main() {
	pata_init();

	printMats(pata_Material_Wood);
	printMats(pata_Material_Meat);
	printMats(pata_Material_Ore);
	printMats(pata_Material_Metal);
	printMats(pata_Material_Veg);
	printMats(pata_Material_Stew);

	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 0) << "\n";
	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 1) << "\n";
	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 2) << "\n";
	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 3) << "\n";
	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 4) << "\n";
	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 5) << "\n";
	std::cout << pata_get_patapon_string(pata_Unit_String_Kanji, 6) << "\n";
	return 0;
}
