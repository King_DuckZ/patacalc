/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id049F7F6C355D48EAB9E6DA18437DF612
#define id049F7F6C355D48EAB9E6DA18437DF612

#include "materials.h"
#include "patapon.h"
#include "auto_impexp/patahelper.h"

#if defined(__cplusplus)
extern "C" {
#endif

PATAHELPER_IMPEXP void pata_init ( void );

#if defined(__cplusplus)
}
#endif

#endif
