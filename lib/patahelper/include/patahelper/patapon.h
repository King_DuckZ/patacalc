/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idBF2D564496FC476FA161575A1C776282
#define idBF2D564496FC476FA161575A1C776282

#include "auto_impexp/patahelper.h"

#if defined(__cplusplus)
enum pata_Units {
	pata_Unit_Hatapon = 0,
	pata_Unit_Yaripon = 1,
	pata_Unit_Tatepon = 2,
	pata_Unit_Yumipon = 3,
	pata_Unit_Kibapon = 4,
	pata_Unit_Dekapon = 5,
	pata_Unit_Megapon = 6
};
enum pata_Unit_Strings {
	pata_Unit_String_Romaji = 0,
	pata_Unit_String_Kanji = 1,
	pata_Unit_String_Desc = 2,
	pata_Unit_String_Furigana = 3
};
#else
#	define pata_Unit_Hatapon 0
#	define pata_Unit_Yaripon 1
#	define pata_Unit_Tatepon 2
#	define pata_Unit_Yumipon 3
#	define pata_Unit_Kibapon 4
#	define pata_Unit_Dekapon 5
#	define pata_Unit_Megapon 6
#	define pata_Unit_String_Romaji 0
#	define pata_Unit_String_Kanji 1
#	define pata_Unit_String_Desc 2
#	define pata_Unit_String_Furigana 3
#endif

#if defined(__cplusplus)
extern "C" {
#endif

PATAHELPER_IMPEXP const char* pata_get_patapon_string ( int parString, int parPatapon );

#if defined(__cplusplus)
}
#endif

#endif
