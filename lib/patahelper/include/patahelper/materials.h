/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idB7080A688A4C482393F04781981AFD8E
#define idB7080A688A4C482393F04781981AFD8E

#include "auto_impexp/patahelper.h"

#if defined(__cplusplus)
enum pata_Materials {
	pata_Material_Wood = 0,
	pata_Material_Meat = 1,
	pata_Material_Ore = 2,
	pata_Material_Metal = 3,
	pata_Material_Veg = 4,
	pata_Material_Stew = 5
};
#else
#	define pata_Material_Wood 0
#	define pata_Material_Meat 1
#	define pata_Material_Ore 2
#	define pata_Material_Metal 3
#	define pata_Material_Veg 4
#	define pata_Material_Stew 5
#endif

#if defined(__cplusplus)
extern "C" {
#endif

PATAHELPER_IMPEXP const char* pata_get_material_desc ( int parType, int parLevel );
PATAHELPER_IMPEXP const char* pata_get_material_cat_desc ( int parType );

#if defined(__cplusplus)
}
#endif

#endif
