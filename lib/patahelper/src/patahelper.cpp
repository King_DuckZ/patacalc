/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "auto_impexp/patahelper.h"
#include "i18n.h"
#include "patahelperConfig.h"

extern "C" PATAHELPER_IMPEXP void pata_init ( void );

extern "C" void pata_init() {
	std::setlocale(LC_ALL, "");
	bindtextdomain(I18N_DOMAIN, I18N_LOCALE_DIR);
	textdomain(I18N_DOMAIN);
}
