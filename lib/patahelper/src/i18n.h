/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id32BC342595D24F4AA07D4D0CE4F13287
#define id32BC342595D24F4AA07D4D0CE4F13287

#if defined(__cplusplus)
#include <clocale>
#else
#include <locale.h>
#endif

#include <libintl.h>

#define _(String) gettext(String)
#define gettext_noop(String) String
#define N_(String) gettext_noop(String)

#endif
