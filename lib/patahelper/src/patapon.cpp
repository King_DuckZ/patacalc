/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "patapon.h"
#include "materials.h"
#include "i18n.h"
#include <cstdint>
#include <stdexcept>
#include <ciso646>

namespace {
	struct NameEtymology {
		const char* const kanji;
		const char* const furigana;
		const char* const romaji;
		const char* const desc;
	};

	struct Sats {
		const uint8_t hp;

	};

	struct Patapon {
		const NameEtymology name;
		const uint8_t base_cost;
		const uint8_t base_cost_veg;
		const uint8_t mat1_type;
		const uint8_t mat2_type;
	};

	const Patapon g_kibapon {
		{ //name
			u8"騎馬",
			u8"きばぽん",
			u8"kibapon",
			N_("horse-rider")
		},
		20,
		0,
		static_cast<uint8_t>(pata_Material_Ore),
		static_cast<uint8_t>(pata_Material_Ore)
	};

	const Patapon g_tatepon {
		{ //name
			u8"盾",
			u8"たてぽん",
			u8"tatepon",
			N_("shield")
		},
		12,
		10,
		static_cast<uint8_t>(pata_Material_Meat),
		static_cast<uint8_t>(pata_Material_Ore)
	};

	const Patapon g_yaripon {
		{ //name
			u8"槍",
			u8"やりぽん",
			u8"yaripon",
			N_("spear")
		},
		8,
		0,
		static_cast<uint8_t>(pata_Material_Meat),
		static_cast<uint8_t>(pata_Material_Wood)
	};

	const Patapon g_yumipon {
		{ //name
			u8"弓",
			u8"ゆみぽん",
			u8"yumipon",
			N_("bow")
		},
		15,
		0,
		static_cast<uint8_t>(pata_Material_Wood),
		static_cast<uint8_t>(pata_Material_Ore)
	};

	const Patapon g_dekapon {
		{ //name
			u8"でかい",
			u8"でかぽん",
			u8"dekapon",
			N_("gargantuan")
		},
		25,
		20,
		static_cast<uint8_t>(pata_Material_Metal),
		static_cast<uint8_t>(pata_Material_Meat)
	};

	const Patapon g_megapon {
		{ //name
			u8"メガ",
			u8"めがぽん",
			u8"megapon",
			N_("mega")
		},
		30,
		0,
		static_cast<uint8_t>(pata_Material_Metal),
		static_cast<uint8_t>(pata_Material_Wood)
	};

	const Patapon g_hatapon {
		{ //name
			u8"旗",
			u8"はたぽん",
			u8"hatapon",
			N_("flag")
		},
		0,
		0,
		0,
		0
	};

	template <typename T, typename F>
	T get_patapon_part (int parPatapon, F parPartGetter) {
		switch (parPatapon) {
		case pata_Unit_Hatapon:
			return parPartGetter(g_hatapon);
		case pata_Unit_Yaripon:
			return parPartGetter(g_yaripon);
		case pata_Unit_Tatepon:
			return parPartGetter(g_tatepon);
		case pata_Unit_Yumipon:
			return parPartGetter(g_yumipon);
		case pata_Unit_Kibapon:
			return parPartGetter(g_kibapon);
		case pata_Unit_Dekapon:
			return parPartGetter(g_dekapon);
		case pata_Unit_Megapon:
			return parPartGetter(g_megapon);
		default:
			throw std::runtime_error("Bad parameter");
		}
	}
} //unnamed namespace

extern "C" const char* pata_get_patapon_string (int parString, int parPatapon) {
	if (parPatapon < 0 or parPatapon > 6)
		return nullptr;
	if (parString < 0 or parString > 3)
		return nullptr;

	try {
		const char* retval;
		switch (parString) {
		case pata_Unit_String_Romaji:
			retval = get_patapon_part<const char*>(parPatapon, [](const Patapon& parPata) { return parPata.name.romaji; });
			break;
		case pata_Unit_String_Kanji:
			retval = get_patapon_part<const char*>(parPatapon, [](const Patapon& parPata) { return parPata.name.kanji; });
			break;
		case pata_Unit_String_Desc:
			retval = get_patapon_part<const char*>(parPatapon, [](const Patapon& parPata) { return parPata.name.desc; });
			retval = _(retval);
			break;
		case pata_Unit_String_Furigana:
			retval = get_patapon_part<const char*>(parPatapon, [](const Patapon& parPata) { return parPata.name.furigana; });
			break;
		default:
			retval = nullptr;
		}
		return retval;
	}
	catch (const std::exception&) {
		return nullptr;
	}
}
