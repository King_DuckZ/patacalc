/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "materials.h"
#include "patahelperConfig.h"
#include "i18n.h"
#include <ciso646>

namespace {
	const char* g_wood [4] = {N_("Banal branch"), N_("Cherry tree"), N_("Hinoki"), N_("Super cedar")};
	const char* g_meat [4] = {N_("Leather meat"), N_("Tender meat"), N_("Dream meat"), N_("Mystery meat")};
	const char* g_ore  [4] = {N_("Stone"), N_("Hard iron"), N_("Tytanium"), N_("Mytheerial")};
	const char* g_metal[4] = {N_("Sloppy alloy"), N_("Hard alloy"), N_("Awesome alloy"), N_("Magic alloy")};
	const char* g_veg  [4] = {N_("Eyeball cabbage"), N_("Crying carrot"), N_("Predator pumpkin"), N_("Hazy shroom")};
	const char* g_stew [4] = {N_("Gnarly stew"), N_("Tasty stew"), N_("King's stew"), N_("Divine stew")};
} //unnamed namespace

extern "C" const char* pata_get_material_desc (int parType, int parLevel) {
	if (parLevel < 1 or parLevel > 4)
		return nullptr;

	switch (parType) {
	case pata_Material_Wood:
		return _(g_wood[parLevel - 1]);
	case pata_Material_Meat:
		return _(g_meat[parLevel - 1]);
	case pata_Material_Ore:
		return _(g_ore[parLevel - 1]);
	case pata_Material_Metal:
		return _(g_metal[parLevel - 1]);
	case pata_Material_Veg:
		return _(g_veg[parLevel - 1]);
	case pata_Material_Stew:
		return _(g_stew[parLevel - 1]);
	default:
		return nullptr;
	}
}

extern "C" const char* pata_get_material_cat_desc (int parType) {
	switch (parType) {
	case pata_Material_Wood:
		return _("Wood");
	case pata_Material_Meat:
		return _("Meat");
	case pata_Material_Ore:
		return _("Ore");
	case pata_Material_Metal:
		return _("Metal");
	case pata_Material_Veg:
		return _("Vegetable");
	case pata_Material_Stew:
		return _("Stew");
	default:
		return nullptr;
	}
}
