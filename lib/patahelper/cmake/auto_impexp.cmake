cmake_minimum_required(VERSION 2.8.12 FATAL_ERROR)

function (auto_impexp TARGET_NAME)
	string(MAKE_C_IDENTIFIER "${TARGET_NAME}" TARGETNAME_CLEAN)
	string(TOUPPER "${TARGETNAME_CLEAN}" TARGETNAME_UPPERCASE)

	get_target_property(PROG_DEFINE_SYMBOL ${TARGET_NAME} DEFINE_SYMBOL)
	if (PROG_DEFINE_SYMBOL STREQUAL "PROG_DEFINE_SYMBOL-NOTFOUND")
		set(PROG_DEFINE_SYMBOL "${TARGETNAME_CLEAN}_EXPORTS")
	endif()

	set (OUT_PATH "${CMAKE_BINARY_DIR}/include/auto_impexp")
	set (INC_PATH "${CMAKE_BINARY_DIR}/include")
	file (MAKE_DIRECTORY "${OUT_PATH}")
	set (FULL_OUT_PATH "${OUT_PATH}/${TARGETNAME_CLEAN}.h")

	set (FILE_CONTENT "/*
	Copyright 2014 Michele \"King_DuckZ\" Santullo

	This file is part of PataCalc.

	PataCalc is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	PataCalc is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with PataCalc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id25445ECEE94A4FDD_${TARGETNAME_CLEAN}
#define id25445ECEE94A4FDD_${TARGETNAME_CLEAN}

#define ${TARGETNAME_UPPERCASE}_$<TARGET_PROPERTY:${TARGET_NAME},TYPE>

#if defined(_MSC_VER)
#	define INTERN_EXPORT_${TARGETNAME_CLEAN}_ __declspec(dllexport)
#	define INTERN_IMPORT_${TARGETNAME_CLEAN}_ __declspec(dllimport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define INTERN_EXPORT_${TARGETNAME_CLEAN}_ __attribute__((visibility(\"default\")))
#	define INTERN_IMPORT_${TARGETNAME_CLEAN}_
#else
#	error \"Unsupported compiler\"
#endif

#if defined(${TARGETNAME_UPPERCASE}_SHARED_LIBRARY)
#	if defined(${PROG_DEFINE_SYMBOL})
#		define ${TARGETNAME_UPPERCASE}_IMPEXP INTERN_EXPORT_${TARGETNAME_CLEAN}_
#	else
#		define ${TARGETNAME_UPPERCASE}_IMPEXP INTERN_IMPORT_${TARGETNAME_CLEAN}_
#	endif
#else
#	define ${TARGETNAME_UPPERCASE}_IMPEXP
#endif

#undef ${TARGETNAME_UPPERCASE}_$<TARGET_PROPERTY:${TARGET_NAME},TYPE>

#endif
")

	unset(PROG_DEFINE_SYMBOL)
	unset(TARGETNAME_CLEAN)
	unset(TARGETNAME_UPPERCASE)
	if ("${CMAKE_CURRENT_LIST_FILE}" IS_NEWER_THAN "${FULL_OUT_PATH}")
		file(GENERATE
			OUTPUT ${FULL_OUT_PATH}
			CONTENT "${FILE_CONTENT}"
		)
	endif()

	unset(FILE_CONTENT)
	set(AUTO_IMPEXP_INCLUDE "${INC_PATH}" PARENT_SCOPE)
	unset(INC_PATH)
	unset(OUT_PATH)
endfunction(auto_impexp)
